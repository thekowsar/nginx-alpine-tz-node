FROM nginx:1.21.0-alpine
MAINTAINER  Kowsar thekowsar@gmail.com

RUN apk update
RUN apk add --no-cache tzdata
ENV TZ Asia/Dhaka

# Install nvm with node and npm
RUN apk add --no-cache --repository http://nl.alpinelinux.org/alpine/edge/main libuv \
    && apk add --no-cache --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/main nodejs=14.17.1-r0 npm=7.17.0-r0 \
    && apk add --no-cache --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community yarn=1.22.10-r0 \
    && echo "NodeJS Version:" "$(node -v)" \
    && echo "NPM Version:" "$(npm -v)" \
    && echo "Yarn Version:" "$(yarn -v)"


# docker build --tag=nginx-alpine-node:1.1 .
# docker tag nginx-alpine-node:1.1 kowsar/nginx-alpine-node:1.1
# docker push kowsar/nginx-alpine-node:1.1
